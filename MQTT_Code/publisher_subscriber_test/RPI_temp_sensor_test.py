import paho.mqtt.client as mqtt
import RPi.GPIO as GPIO
import time
import Freenove_DHT as DHT

DHTPin = 11

dht = DHT.DHT(DHTPin)   #create a DHT class object
sumCnt = 0

mqttBroker = "mqtt.eclipseprojects.io"
client = mqtt.Client("Client 1")
client.connect(mqttBroker)
print("Connected to the MQTT broker.")
  
while True:
    try:
        sumCnt += 1         #counting number of reading times
        chk = dht.readDHT11()     #read DHT11 and get a return value. Then determine whether data read is normal according to the return value.
        print ("The sumCnt is : %d, \t chk    : %d"%(sumCnt,chk))
        if (chk is dht.DHTLIB_OK):      #read DHT11 and get a return value. Then determine whether data read is normal according to the return value.
            print("DHT11,OK!")
        elif(chk is dht.DHTLIB_ERROR_CHECKSUM): #data check has errors
            print("DHTLIB_ERROR_CHECKSUM!!")
        elif(chk is dht.DHTLIB_ERROR_TIMEOUT):  #reading DHT times out
            print("DHTLIB_ERROR_TIMEOUT!")
        else:               #other errors
            print("Other error!")
        client.publish("Temperature", dht.temperature)
        print("Temperature of", str(dht.temperature), "published.")
        time.sleep(2)
    except(KeyboardInterrupt):
        client.disconnect(mqttBroker)
        print("Connection disconnected. ")
        GPIO.cleanup()
        break
    