import paho.mqtt.client as mqtt
import random
import time

mqttBroker = "mqtt.eclipseprojects.io"
client = mqtt.Client("Client 1")
client.connect(mqttBroker)
print("Connected to the MQTT broker.")

while True:
    try:
        randomNumber = random.randint(0,101)
        client.publish("Random Number", randomNumber)
        print("Random number", str(randomNumber), "published.")
        time.sleep(2)
    except(KeyboardInterrupt):
        client.disconnect(mqttBroker)
        print("Connection disconnected. ")
        break
    