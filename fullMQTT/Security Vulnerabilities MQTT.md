Security Vulnerabilities MQTT: 

1.	The first Vulnerability we in the group have thought about is that if someone has been able to access our MQTT network they would be able to send false data to the broker. This could be a huge safety risk as the potential hacker could either send false data about the sensors or in our specific instance thermal camera that there are no issues when in fact there could be and this could cause power failures and big issues for the data center. On the other hand the hackers could send false data that there are power issues when there are none and this could create panic and workload when in fact all systems are running as predicted.  We assess this to be a safety risk of 10 out of 10 as in both instances this would cause huge problems for the data center. 

2.	Another possible issue we actually thought about because of the international workshop day with fontys university, was if people were able to publish to the broker and where then able to completely flood the server possibly causing a crash this could create a serious headache rebooting the broker and making sure the systems is up and running again, much like the workshop day where 1 or 2 students completely flooded the hosts broker causing crashes and ip bans from the mqtt test broker. We have assessed this to be a safety risk of 7/10 as it would cause a lot of work and possible down time where system failures could be undetected. 

3.	Another Security issue we could see with MQTT is if passwords and other sensitive information is unencrypted and hackers are able to “sniff” on the network and capture our data packets. Then they would be able to gain access to private information and possibly other parts of the data center system if the security codes or passwords are in use multiple places. This could for example be other companies or foreign countries trying to gain access to the schematics or other system information in order to be able to copy and create their own. We see this risk as a 8/10 as sensitive information like passwords or security codes could lead to even bigger issues further down the line. 











Possible solutions for the vulnerabilities we have thought about: 

1.	The solution for our first and second vulnerability would be from the given article about X.509 authentication and making sure that the connected devices are from clients with the correct permission, if the broker does not accept X.509 authentication then this could also be done with SSH keys. We could also make sure that only MAC addresses with the correct permission can publish to the broker making sure that other systems or persons are not able to send false data to the broker and cause chaos and confusion. If we wanted to keep the system even more private then we could restrict it to local network causing possible hackers to need local access to be able to do anything, however this could reduce the point of using MQTT. 

2.	For the third Vulnerability it is crucial that data packets etc. are encrypted in some way or from so that outsiders are unable to see what is inside and possibly gain access or credentials via the data packets. This could also be done via the X.509 authentication as it requires encryption of the TLS which is related to the TCP/IP. Or via MQTT payload encryption.  
