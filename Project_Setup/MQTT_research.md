<h1>What is the MQTT and what is it used for?</h1> 

***MQTT is a lightweight protocol for transmitting data between machines and can be compared with http.***

<h2>How does it work?</h2>


Following points:

- **Broker**

- **Publishing and Subscribing**

- **Topics**

- **Quality of service**

<h2>Broker:</h2>
All devices or clients communicate through a middleman - this is a server or broker in MQTT terms. The broker can be installed in your PC, Mac, most Linux systems including Raspbian in Raspberry PI, in container installations on the cloud.

<h2>Publishing and Subscribing:</h2>

Client devices transfer or receive information either from the broker or by subscribing to specific topics. They can also publish messages of the specific topic to the broker. One client does not stay in direct contact with another client the whole time since any connection goes through a broker. Also a client is not limited to just publishing or subscribing. All kinds of devices can access the clients such as micro controllers like Arduinos, or ESP's, as well as computers. 
<h2>Topics:</h2>

Topics are a way of categorizing the kind of messages that might be sent. Clients can transmit  messages of a specific topic and can also subscribe to specific topics. The broker gets all messages and forwards messages to all clients that subscribe to that topic. Topics are basically high keek all  categories. If you want a device to get messages from multiple topics, you can use wildcards. Single level wildcards are indicated with a plus icon which replaces one topic level for example the room level then you will get all temperature data from any room on the third floor. Multi level wildcards are indicated with a hashtag and that replaces any deeper topic level from the wildcard onwards . So now you get all data from the third floor.

If there are no subscribers to a topic at all, the broker discards the message, but the publisher can tell the broker to keep the messages until the client subscribes to the topic that makes sense if you want a new client to get the latest value right when it subscribes.

<h2>Quality of service (QoS):</h2>
Depending on your needs for a message being definitely received or definitely received just once, there are three different types of QoS settings available for messages you send:

- Quality of Service 0 - that's the lightest when it comes to network usage. The message is sent once regardless of any feedback from the broker.

- Quality of Service 1 - the publisher will send its message over and over again until it gets a confirmation message from the broker. This can lead 
to multiple deliveries.

- Quality of Service 2 - is a four-part handshake. This guarantees that each message is received only once by confirming their seat back and forth because MQTT is so lightweight in terms of data size and processing power it is available to a vast majority of machines even with very low resources. Just as a footnote data sizes can be as small as 2 bytes but can also contain up to 256 megabytes. 

<h3>Where is it useful and what companies use it?</h3>

- MQTT is used in a large variety of use cases and industries: Logistics, Automotive, Manufacturing, Smart Home, Oil and Gas, Consumer Products, Transportation.
<h3>Some example from the real world industry:</h3>

- HIveMQ: It’s a BMW Car-Sharing application. The basic idea is that for 2030 the 60% of the Earth population will live in cities.  So the BMW company also tries to focus the mobility services for the urban lifestyle such as car-sharing and other ideas. 

- Logistics: For drones communication. Transporting goods and medical samples with drones can reduce the time between the hospitals and the testing laboratories. 

- Manufacturing: One of the largest industrial turkish companies - Celikler Holding - had started to use MQTTRoute to monitor power generation from multiple power plants. 

<h2>How is it relevant to our studies (IT-Technology)?</h2>

Based on MQTT is a lightweight protocol for transmitting data between machines and sensors, it is very relevant to our IT-technology studies. To know an overview how the IOT network works for example in smart homes or in factories. Or to be able to set up and manage a zigbee network. 
