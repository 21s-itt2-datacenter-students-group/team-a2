# What are microservices?

Microservices' core premise is that breaking down certain forms of applications into smaller, composable parts that work together makes them simpler to construct and maintain. The program is then simply the sum of its constituent parts, which are continuously developed and separately maintained. This differences from a conventional "monolithic" application, which is developed in one piece. Applications built as a set of modular components are easier to understand, easier to test, and most importantly easier to maintain over the life of the application. It allows companies to achieve much higher flexibility and vastly reduce the time needed to implement production changes. This method has proven to be superior, especially for large enterprise applications developed by teams of developers from different geographical and cultural backgrounds.

## There are other benefits:

-	Developer independence: Small teams work in parallel and can iterate faster than large teams.
-	Isolation and resilience: If a component dies, you spin up another while and the rest of the application continues to function.
-	Scalability: Smaller components take up fewer resources and can be scaled to meet increasing demand of that component only.
-	Lifecycle automation: Individual components are easier to fit into continuous delivery pipelines and complex deployment scenarios not possible with monoliths.
-	Relationship to the business: Microservice architectures are split along business domain boundaries, increasing independence and understanding across the organization.

The most popular concept of microservices is that each one has an API endpoint, which is usually but not always a stateless REST API that can be accessed over HTTP(S) just like a regular web page. Since this method of accessing microservices only requires tools and methods that many developers are already familiar with, it makes them simple to consume for developers.

# Why is open source important for microservices?

When you develop your programs from the ground up to be functional and composable, you can use drop-in components in many areas where proprietary solutions were previously needed, either due to component licensing or specialized specifications.
Many microservices can be off-the-shelf open source tools, and there are a range of open source projects that adopt cross-cutting microservice architecture specifications like authentication, service discovery, logging and monitoring, load balancing, scaling, and so many others.


Source : https://opensource.com/resources/what-are-microservices
